<?php 
header('Content-Type: text/html; charset=utf-8');
require_once 'config.php';

if(	isset($_POST['name']) && isset($_POST['surname']) && isset($_POST['gender']) && isset($_POST['age']) && isset($_POST['grouping']) && isset($_POST['department']) && isset($_POST['city']) && isset($_POST['country'])){

	$name = htmlentities(mysqli_real_escape_string($link, $_POST['name']));
	$surname = htmlentities(mysqli_real_escape_string($link, $_POST['surname']));
	$gender = htmlentities(mysqli_real_escape_string($link, $_POST['gender']));
	$age = (int)$_POST['age'];
	$grouping = htmlentities(mysqli_real_escape_string($link, $_POST['grouping']));
	$department = htmlentities(mysqli_real_escape_string($link, $_POST['department']));
	$city = htmlentities(mysqli_real_escape_string($link, $_POST['city']));
	$country = htmlentities(mysqli_real_escape_string($link, $_POST['country']));

	$query = "INSERT INTO people (name, surname, gender, age, grouping, department, city, country, date_added) VALUE ('$name', '$surname', '$gender', '$age', '$grouping', '$department', '$city', '$country', NOW())";
	$result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link));
	
	if($result) {
		echo "<span class='success'>Success</span>";
	}
	mysqli_close($link);
} else {
	$name = "";
	$surname = "";
	$gender = "";
	$age = "";
	$grouping = "";
	$department = "";
	$city = "";
	$country = "";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Test task for Autoenterprise</title>
	<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="form">
		<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
			<input type="text" name="name" value="<?php echo $name;?>" placeholder="Name" required><br>
			<input type="text" name="surname" value="<?php echo $surname;?>" placeholder="Surname" required><br>
			<input type="text" name="gender" value="<?php echo $gender;?>" placeholder="Gender" required><br>
			<input type="text" name="age" value="<?php echo $age;?>" placeholder="Age" required><br>
			<input type="text" name="grouping" value="<?php echo $grouping;?>" placeholder="Group" required><br>
			<input type="text" name="department" value="<?php echo $department;?>" placeholder="Department" required><br>
			<input type="text" name="city" value="<?php echo $city;?>" placeholder="City" required><br>
			<input type="text" name="country" value="<?php echo $country;?>" placeholder="Country" required><br>
			<button type="submit" value="submit">Submit</button><br>
			<button type="reset" value="reset">Reset</button>
		</form>
	</div>
</body>
</html>