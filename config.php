<?php
define('HOST', 'localhost');
define('DATABASE', 'autoenterprise');
define('USER', 'root');
define('PASSWORD', '');

$link = mysqli_connect(HOST, USER, PASSWORD, DATABASE) or die("Ошибка " . mysqli_error($link)); 
$link->set_charset("utf8");

define('DIR_ROOT', __DIR__ . '/');
define('VIEW', DIR_ROOT .'/view/');
define('HTTP_SERVER', 'http://http://autoenterprise.loc');
define('IMAGE', HTTP_SERVER .'/img/');
?>