-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Апр 21 2018 г., 09:47
-- Версия сервера: 10.1.19-MariaDB
-- Версия PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `autoenterprise`
--

-- --------------------------------------------------------

--
-- Структура таблицы `people`
--

CREATE TABLE `people` (
  `id` int(11) NOT NULL,
  `name` varchar(35) NOT NULL,
  `surname` varchar(35) NOT NULL,
  `gender` varchar(15) NOT NULL,
  `age` int(2) UNSIGNED NOT NULL,
  `grouping` varchar(50) NOT NULL,
  `department` varchar(50) NOT NULL,
  `city` varchar(35) NOT NULL,
  `country` varchar(35) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `people`
--

INSERT INTO `people` (`id`, `name`, `surname`, `gender`, `age`, `grouping`, `department`, `city`, `country`, `date_added`) VALUES
(14, 'sdd', 'Sidash', 'male', 24, 'werf', 'wer', 'er', 'Ukraine', '2018-04-21 10:13:15'),
(15, 'sdd', 'Sidash', 'male', 24, 'werf', 'wer', 'er', 'Ukraine', '2018-04-21 10:14:53'),
(16, 'sdd', 'Sidash', 'male', 12, 'werf', 'front-end', 'Kharkov', 'Ukraine', '2018-04-21 10:15:32'),
(17, 'sdd', 'Sidash', 'male', 12, 'werf', 'front-end', 'Kharkov', 'Ukraine', '2018-04-21 10:15:55'),
(18, 'sdd', 'Sidash', 'male', 12, 'werf', 'front-end', 'Kharkov', 'Ukraine', '2018-04-21 10:16:16'),
(19, 'sdd', 'Sidash', 'male', 12, 'werf', 'front-end', 'Kharkov', 'Ukraine', '2018-04-21 10:21:45'),
(20, 'Eugene', 'Sidash', 'male', 24, 'werf', 'front-end', 'Харьков', 'Ukraine', '2018-04-21 10:29:42'),
(21, 'Eugene', 'Sidash', 'male', 24, 'werf', 'front-end', 'Харьков', 'Ukraine22', '2018-04-21 10:30:14'),
(22, 'sdfgh', 'yy', 'yy', 7, 'hh', 'h', 'hh', 'h', '2018-04-21 10:30:40'),
(23, 'Eugene', 'Sidash', 'male', 24, 'werf', 'front-end', 'Kharkov', 'Ukraine', '2018-04-21 10:44:54');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `people`
--
ALTER TABLE `people`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
